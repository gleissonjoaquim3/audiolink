audiolink (0.05-4) unstable; urgency=medium

  * New maintainer. (Closes: #831544)
  * Run wrap-and-sort.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Created VCS fields.
      - Removed unnecessary greater-than versioned dependencies.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/docs: added missing documentation in Documentation/*.html from
    upstream. Consequently:
      - debian/doc-base: created to register the documentation.
  * debian/patches/:
      - Forwarded all patches.
      - 010_debian-changes.patch:
          ~ Added a header.
          ~ Changed the numeric prefix.
      - 020_fix-spelling-error: created to fix spelling error.
  * debian/README.Debian: no longer needed. Removed.
  * debian/rules:
      - Added a export line for DH_VERBOSE.
      - Added DEB_BUILD_MAINT_OPTIONS variable to improve GCC hardening.
      - Removed a override_dh_installchangelogs because the RELEASE_NOTES
        already has the same information.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform a CI test.
  * debian/upstream/metadata: created.
  * debian/watch: created.

 -- Gleisson Jesuino Joaquim Cardoso <gleissoncg2@gmail.com>  Tue, 11 Aug 2020 20:18:21 -0300

audiolink (0.05-3) unstable; urgency=medium

  * QA upload.
  * Depends/Recommends: default-mysql-* | virtual-mysql-*.
    (Closes: #848428, #732876)

 -- Andreas Beckmann <anbe@debian.org>  Wed, 11 Jan 2017 19:22:00 +0100

audiolink (0.05-2) unstable; urgency=medium

  * QA upload.
  * Switch to dh.
  * Switch to "3.0 (quilt)" source format.
  * Raise debhelper compat level to 9. Closes: #817368.
  * Package is "Arch: all", put everything in Build-Depends.
  * Package is orphaned (Bug #831544), set Maintainer to "Debian QA Group".

 -- Santiago Vila <sanvila@debian.org>  Sun, 11 Sep 2016 20:09:38 +0200

audiolink (0.05-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Use Ogg::Vorbis::Header::PurePerl instead of Ogg::Vorbis::Header.
    (Closes: #655407)

 -- Tim Retout <diocles@debian.org>  Mon, 09 Apr 2012 20:48:40 +0100

audiolink (0.05-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Move debhelper to Build-Depends, to get rid of lintian error
    preventing upload.
  * Use File:Temp to generate a temporary file (Closes: #496433).

 -- Sebastien Delafond <seb@debian.org>  Wed, 03 Sep 2008 14:53:36 -0700

audiolink (0.05-1) unstable; urgency=low

  * Initial Release (closes: #222450)
  * Sponsored by Gunnar Wolf <gwolf@debian.org>

 -- Gunnar Wolf <gwolf@debian.org>  Tue,  3 Feb 2004 14:31:50 -0600
