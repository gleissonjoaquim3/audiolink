$Id: tools_used.txt,v 1.1.1.1 2003/09/11 08:14:29 amitshah Exp $

AudioLink

Tools to be used:

1) MySQL as the database backend
2) Perl as the programming language
3) Command line Interface and KDE/Qt for the UI


Why:
1) MySQL: no reason, could've used PostgreSQL too, but I like the name
   MySQL better. In the future, we could add support for PostgreSQL too.

2) Perl: I don't know perl yet and wanted to learn it...

3) CLI: Ofcourse, each app should have a CLI
   KDE/Qt: My preferred DE, and also, I'll get to learn Qt programming
