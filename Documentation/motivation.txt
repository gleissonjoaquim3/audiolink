$Id: motivation.txt,v 1.1.1.1 2003/09/11 08:14:29 amitshah Exp $

AudioLink

This project started with my need of searching for files on my local
machine, be it music or any stored information in .txt, .html, .pdf
formats. The main goal of the software is to make searching for
_content_ on local file systems (or remote file systems mounted in the
local namespace) easier. This differs from other search tools, which
look for files, not content. You can't use traditional tools like grep
to search for songs or a particular artist, for example.

The project could further be improved upon to include a LAN crawler,
which will sniff on NFS, SMB, FTP among other protocols to collect
information on the files residing on other machines as well.
